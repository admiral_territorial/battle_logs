import { IRow, BattleLog } from '../../src/analysis/BattleLog';
import {describe, expect, test} from '@jest/globals';
import fs from 'fs';

function load_mock_data(filename:string):Array<Array<IRow>> {
  let data = fs.readFileSync(filename, 'utf8');
  return JSON.parse(data) as Array<Array<IRow>>
} 
describe('1 on 1 combat', () => {
  let one_on_one = load_mock_data("tests/mock.tables.1on1.json")
  let battle_log = new BattleLog(one_on_one)
  test('First table should contain player names', () => {
    expect(battle_log.tables[0][0]['Player Name']).toBeDefined();
  });
  test('Two player ships are returned', () => {
    expect(battle_log.ships.length).toBe(2);
  });
  test('Player 1 ship is not enemy', () => {
    expect(battle_log.ships[0].is_enemy).toBeFalsy();
  });
  test('Player 2 ship is enemy', () => {
    expect(battle_log.ships[1].is_enemy).toBeTruthy();
  });
  test('Player 1 ship is dead', () => {
    expect(battle_log.ships[0].is_alive).toBeFalsy();
  });
  test('Player 2 ship is alive', () => {
    expect(battle_log.ships[1].is_alive).toBeTruthy();
  });
});

describe('Armada combat', () => {
  let armada = load_mock_data("tests/mock.tables.armada.json")
  let battle_log = new BattleLog(armada)
  test('First table should contain player names', () => {
    expect(battle_log.tables[0][0]['Player Name']).toBeDefined();
  });
  test('Six player ships are returned', () => {
    expect(battle_log.ships.length).toBe(6);
  });
  test('Player 1 ship is not enemy', () => {
    expect(battle_log.ships[0].is_enemy).toBeFalsy();
  });
  test('Player 2 ship is not enemy', () => {
    expect(battle_log.ships[1].is_enemy).toBeFalsy();
  });
  test('Player 3 ship is not enemy', () => {
    expect(battle_log.ships[2].is_enemy).toBeFalsy();
  });
  test('Player 4 ship is not enemy', () => {
    expect(battle_log.ships[3].is_enemy).toBeFalsy();
  });
  test('Player 5 ship is not enemy', () => {
    expect(battle_log.ships[4].is_enemy).toBeFalsy();
  });
  test('Ship 1 player is boredtotears', () => {
    expect(battle_log.ships[0].player).toBe("boredtotears");
  });
  test('Ship 2 player is Territorial', () => {
    expect(battle_log.ships[1].player).toBe("Territorial");
  });
  test('Ship 3 player is KingHookerPauly', () => {
    expect(battle_log.ships[2].player).toBe("KingHookerPauly");
  });
  test('Ship 4 player is TheJohans', () => {
    expect(battle_log.ships[3].player).toBe("TheJohans");
  });
  test('Ship 5 player is EmersentheQueer', () => {
    expect(battle_log.ships[4].player).toBe("EmersentheQueer");
  });
  test('Ship 6 player is Marauder Haven', () => {
    expect(battle_log.ships[5].player).toBe("Marauder Haven");
  });
});


describe('Solo armada combat', () => {
  let armada = load_mock_data("tests/mock.tables.solo.json")
  let battle_log = new BattleLog(armada)
  test('First table should contain player names', () => {
    expect(battle_log.tables[0][0]['Player Name']).toBeDefined();
  });
  test('Four player ships are returned', () => {
    expect(battle_log.ships.length).toBe(4);
  });
  test('Player 1 ship is not enemy', () => {
    expect(battle_log.ships[0].is_enemy).toBeFalsy();
  });
  test('Player 2 ship is not enemy', () => {
    expect(battle_log.ships[1].is_enemy).toBeFalsy();
  });
  test('Player 3 ship is not enemy', () => {
    expect(battle_log.ships[2].is_enemy).toBeFalsy();
  });

  test('Ship 1 player is Territorial', () => {
    expect(battle_log.ships[0].player).toBe("Territorial");
  });
  test('Ship 2 player is Territorial', () => {
    expect(battle_log.ships[1].player).toBe("Territorial");
  });
  test('Ship 3 player is Territorial', () => {
    expect(battle_log.ships[2].player).toBe("Territorial");
  });
  test('Ship 4 player is Borg Sphere', () => {
    expect(battle_log.ships[3].player).toBe("Borg Sphere");
  });
});