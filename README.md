# Admiral Territorial's Battle Log Tool

This tool's core functionality is implemented as front end code (runs in your browser), so it can be deployed as a static page without a specialized API server.
However, to enable sharing functionalities, a separate API back end is required.

This code is distributed under the *BSD-3-Clause* license, see the LICENSE file. This essentially permits you to do whatever you want with it, except claim you wrote it.
Contributions are welcome.