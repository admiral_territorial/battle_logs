// Load CSV files, or specifically the ones exported by STFC

import {
  ChartData,
  ChartDataset,
  ChartTypeRegistry
  // @ts-ignore    
} from "chart.js"

import ship_mapping from "./ship_mapping.json"
import captain_mapping from "./captain_mapping.json"
import officer_mapping from "./officer_mapping.json"

export interface IRow {
  [key: string]: string;
}

interface IShipCandidate {
  name_candidates?: Array<string>;
  name: string;
  strength: number;
  captain: string;
  officer1: string;
  officer2: string;
  player: string;
  is_enemy: boolean;
  is_alive: boolean;
}

interface INumberRow {
  [key: string]: number;
}

interface IDamageRow {
  [key: string]: number | string;
  total: number;
  isolytic: number;
  critical: number;
  critical_pct: string;
  unmitigated: number;
  unmitigated_isolytic: number;
  mitigated: number;
  mitigated_isolytic: number;
  shield: number;
  hull: number;
  hull_recv: number;
  total_recv: number;
}

export interface IDamageTable {
  [key: string]: IDamageRow;
}
export interface INumberTable {
  [key: string]: INumberRow;
}

interface ITable {
  rows: Array<Array<string>>;
  rows_received: Array<Array<string>>;
  headers: Array<string>;
}

interface IStats {
  total: number
  unmitigated: number
  hull: number
  critical: number
  total_recv: number
  mitigated: number
  hull_recv: number
}

interface IStatsResult {
  per_person: IDamageTable
  attackers: Array<string>
}

interface IDamagePies {
  [key:string]: ChartData<"pie">
}
const background_colors: Array<string> = [
  "#FF8A80",
  "#E040FB",
  "#FF6E40",
  "#448AFF",
  "#18FFFF",
  "#69F0AE",
  "#EEFF41",
  "#FFAB40",
];

function draw_line(): ChartData<"line"> {
  let line: ChartData<"line"> = {
    labels: [] as Array<string>,
    datasets: [{
      backgroundColor: background_colors,
      borderColor: "#121212",
      data: []
    }]
  }
  return line;
}
function bake_pie(set_count:number=1): ChartData<"pie"> {
  let pie: ChartData<"pie"> = {
    labels: [] as Array<string>,
    datasets: []
  }
  for(let i = 0; i < set_count; i++) {
    pie.datasets.push({
      backgroundColor: background_colors,
      borderColor: "#121212",
      data: []
    });  
  }
  return pie;
}

function make_per_person_row(): IDamageRow {
  return {
    total: 0,
    isolytic: 0,
    unmitigated: 0,
    unmitigated_isolytic: 0,
    hull: 0,
    critical: 0,
    total_recv: 0,
    mitigated: 0,
    mitigated_isolytic: 0,
    shield: 0,
    hull_recv: 0,
    critical_pct: "",
    isolytic_pct: "",
    mitigated_pct: ""
  };
}

const NAMES: IRow = {
  "--": "Alliance Starbase"
}
export class BattleLog {
  // so STFC technically exports TSV files rather than CSV, apparently
  delimiter = "\t";
  // windows-like line-endings
  line_ending = "\r\n";
  // file contains multiple tables
  tables: Array<Array<IRow>> = [];
  // we need these mappings to identify ships and officers from the fleet lists
  ability_to_ship: { [key: string]: Array<string> } = ship_mapping;
  ability_to_captain: IRow = captain_mapping;
  ability_to_officer: IRow = officer_mapping;
  // init chart data. For simplicity and avoiding extra data transformations, we're just going to output ChartJS data formats.
  total_damage: ChartData<"pie"> = bake_pie();
  unmitigated_damage: ChartData<"pie"> = bake_pie();
  hull_damage: ChartData<"pie"> = bake_pie();
  damage_per_round: ChartData<"line"> = draw_line();
  damage_breakdown: ChartData<"pie"> = bake_pie(3);
  damage_pies: IDamagePies = {};
  // We know which ships are involved, but not who they belong to.
  ships: Array<IShipCandidate> = [];
  title: string = "";
  summary: ITable = {
    rows: [],
    rows_received: [],
    headers: []
  };
  location = "";
  timestamp = "";
  loaded = false;
  constructor(tables: Array<Array<IRow>> = []) {
    this.tables = tables;
    if (this.tables && this.tables.length > 0) {
      this.location = this.tables[0][0]["Location"];
      this.timestamp = this.tables[0][0]["Timestamp"];
      const stats = this.get_stats();
      this.summary = this.get_summary_table(stats);
      this.ships = this.get_ships();
      this.title = this.get_title(stats);
      this.damage_pies = this.get_damage_pies(stats);
      this.loaded = true;
    }
  }
  from_text(raw_data: string) {
    // tables are separated by an empty line
    if(!raw_data.includes(this.line_ending)) {
      this.line_ending = "\n"
    }
    const tables = raw_data.split(`${this.line_ending}${this.line_ending}`);
    for (const table of tables) {
      let rows: Array<string> = table.split(this.line_ending);
      const header_row = rows.shift() as string
      const header = header_row.split(this.delimiter);
      let parsed_table = [];
      // skip tables that have no rows
      if (rows.length < 1) continue;
      for (const row of rows) {
        let parsed_row: IRow = {};
        const items = row.split(this.delimiter);
        for (const item in items) {
          parsed_row[header[item]] = items[item];
        }
        parsed_table.push(parsed_row);
      }
      this.tables.push(parsed_table);
    }
    this.location = this.tables[0][0]["Location"];
    this.timestamp = this.tables[0][0]["Timestamp"];
    const stats = this.get_stats();
    this.summary = this.get_summary_table(stats);
    this.ships = this.get_ships();
    this.title = this.get_title(stats);
    this.damage_pies = this.get_damage_pies(stats);
    console.log(this.damage_pies);
    this.loaded = true;
  }
  from_file(file: Blob): Promise<BattleLog> {
    let promise = new Promise<BattleLog>((resolve, reject) => {
      let fileReader = new FileReader();
      fileReader.readAsText(file);
      fileReader.onload = () => {
        this.from_text(fileReader.result as string);
        resolve(this as BattleLog);
      };
      fileReader.onerror = () => {
        reject(fileReader.error);
      };
    });
    return promise;
  }
  get_title(stats: IStatsResult): string {
    let target = this.tables[0][this.tables[0].length - 1]["Player Name"]
    target = NAMES[target] || target
    let leader = this.ships[0].player;
    let outcome = this.tables[0][this.tables[0].length - 1]["Outcome"] == "VICTORY" ? "was defeated by" : "defeated"
    let title = `${leader} ${outcome} ${target}`
    return title
  }
  get_ships():Array<IShipCandidate> {
    let ship_candidates: Array<IShipCandidate> = [];
    let player_ship_candidates: Array<IShipCandidate> = [];
    let list_has_candidate = (list:Array<IShipCandidate>, player:string, ship:string) => {
      for(let candidate of list) {
        if(candidate.name == ship && candidate.player == player) {
          return candidate
        }
      }
      return false
    }
    let known_enemy = "";
    for (let table of this.tables) {  
      if (table[0]["Player Name"]) {
        known_enemy = table[table.length - 1]["Player Name"];
      }
      if (table[0]["Fleet Type"]) {
        for (let row of table) {
          let candidate: IShipCandidate = {
            strength: parseInt(row["Attack"]) + parseInt(row["Defense"]) + parseInt(row["Health"]),
            name_candidates: this.ability_to_ship[row["Ship Ability"]],
            name: "",
            captain: this.ability_to_captain[row["Captain Maneuver"]],
            officer1: this.ability_to_officer[row["Officer Two Ability"]],
            officer2: this.ability_to_officer[row["Officer Three Ability"]],
            player: "",
            is_enemy: row["Fleet Type"].includes("Enemy"),
            is_alive: true
          };
          ship_candidates.push(candidate);
        }
        // break;
      }
      if (table[0]['Round']) {
        for (let row of table) {
          if (row['Attacker Name'] !== '' && row['Attacker Name'] !== '--') {
            let existing = list_has_candidate(player_ship_candidates, row['Attacker Name'], row['Attacker Ship']);
            if (existing && row['Type'] == "Combatant Destroyed") {
              existing.is_alive = false;
            }
            if (row['Type'] == 'Attack' && !existing) {
              let candidate: IShipCandidate = {
                strength: 0, //parseInt(row["Attack"]) + parseInt(row["Defense"]) + parseInt(row["Health"]),
                name_candidates: [ row['Attacker Ship'] ],//this.ability_to_ship[row["Ship Ability"]],
                name: row['Attacker Ship'],
                captain: '', //this.ability_to_captain[row["Captain Maneuver"]],
                officer1: '', //this.ability_to_officer[row["Officer Two Ability"]],
                officer2: '', //this.ability_to_officer[row["Officer Three Ability"]],
                player: row['Attacker Name'],
                is_enemy: row["Target - Is Armada?"] === "YES" || known_enemy === row["Attacker Name"],
                is_alive: true
              };
              player_ship_candidates.push(candidate);  
            }
          } 
        }
      }
    }
    player_ship_candidates = player_ship_candidates.sort((a, b) => {
      if (a.is_enemy && !b.is_enemy) return 1;
      if (!a.is_enemy && b.is_enemy) return -1;
      return 0;
    });
    for(let candidate of ship_candidates) {
      for(let player_candidate of player_ship_candidates) {
        if(player_candidate.name == "assigned") continue;
        if(candidate.name == '' && candidate.name_candidates?.includes(player_candidate.name)) {
          candidate.name = player_candidate.name;
          candidate.player = player_candidate.player;
          candidate.is_alive = player_candidate.is_alive;
          player_candidate.name = "assigned"
          continue;
        } else {
          if(candidate.name == "" && player_candidate.is_enemy == true && candidate.is_enemy == true) {
            candidate.name = player_candidate.name;
            candidate.player = player_candidate.player;
            candidate.is_alive = player_candidate.is_alive;
            player_candidate.name = "assigned"
            continue;
          }
        }
      }
    }
    return ship_candidates
  }
  get_summary_table(stats: IStatsResult): ITable {
    let summary_table: ITable = {
      rows: [],
      rows_received: [],
      headers: stats.attackers
    };

    let rows: IRow = {
      "total": "Total Damage",
      "isolytic": "Isolytic Damage",
      "critical": "Critical Damage",
      "critical_pct": "Critical %",
      "isolytic_pct": "Isolytic %",
      "unmitigated": "Unmitigated Damage",
      "unmitigated_isolytic": "Unmitigated Isolytic",
      "hull": "Hull Damage",
    };
    let rows_received: IRow = {
      "total_recv": "Total Damage",
      "mitigated": "Mitigated Damage",
      "mitigated_pct": "Mitigated %",
      "hull_recv": "Hull Damage",
    };

    let number_rows = [
      "total", "unmitigated", "mitigated", "total_recv", "hull_recv", "hull", "critical", "isolytic", "unmitigated_isolytic"
    ];
    for (let row in rows) {
      let row_data: Array<string> = [rows[row] as string];
      for (let person of stats.attackers) {
        if (number_rows.includes(row)) {
          row_data.push(stats.per_person[person][row].toLocaleString("en-uS"));
        } else {
          // row_data.push((stats.per_person[person][row] as number).toFixed(2)) + "%";
          row_data.push(stats.per_person[person][row] as string);
        }
      }
      summary_table.rows.push(row_data);
    }
    for (let row in rows_received) {
      let row_data = [rows_received[row]];
      for (let person of stats.attackers) {
        if (number_rows.includes(row)) {
          row_data.push(stats.per_person[person][row].toLocaleString("en-uS"));
        } else {
          // row_data.push((stats.per_person[person][row] as number).toFixed(2)) + "%";
          row_data.push(stats.per_person[person][row] as string);
        }
      }
      summary_table.rows_received.push(row_data);
    }

    return summary_table;
  }
  get_damage_pies(stats:IStatsResult): IDamagePies {
    let pies:IDamagePies = {};
    console.log("Damage pies");
    for(let person of stats.attackers) {
      let display_person = person.split('(')[0];
      console.log("person", person);
      let big_total = stats.per_person[person].total + stats.per_person[person].isolytic; 
      pies[display_person] = bake_pie(3);
      pies[display_person].labels = [ "Standard Damage", "Isolytic Damage", "Mitigated", "Isolytic Mitigated", "Hull Damage", "Shield", "Isolytic Hull Damage" ]; 
      pies[display_person].datasets[0].data[0] = stats.per_person[person].total;
      pies[display_person].datasets[0].data[1] = stats.per_person[person].isolytic;
      pies[display_person].datasets[0].data[2] = 0;
      pies[display_person].datasets[0].data[3] = 0;
      pies[display_person].datasets[0].data[4] = 0;
      pies[display_person].datasets[0].data[5] = 0;
      pies[display_person].datasets[0].data[6] = 0;
      pies[display_person].datasets[0].weight = 1.0;

      pies[display_person].datasets[1].data[0] = 0;
      pies[display_person].datasets[1].data[1] = 0;
      pies[display_person].datasets[1].data[2] = stats.per_person[person].mitigated;
      pies[display_person].datasets[1].data[3] = stats.per_person[person].mitigated_isolytic;
      pies[display_person].datasets[1].data[4] = 0;
      pies[display_person].datasets[1].data[5] = stats.per_person[person].shield;
      pies[display_person].datasets[1].data[6] = 0;
      pies[display_person].datasets[1].weight = 1.0 / big_total * (stats.per_person[person].mitigated + stats.per_person[person].mitigated_isolytic + stats.per_person[person].shield);

      pies[display_person].datasets[2].data[0] = 0;
      pies[display_person].datasets[2].data[1] = 0; 
      pies[display_person].datasets[2].data[2] = 0; 
      pies[display_person].datasets[2].data[3] = 0; 
      pies[display_person].datasets[2].data[4] = stats.per_person[person].hull; 
      pies[display_person].datasets[2].data[5] = 0; 
      pies[display_person].datasets[2].data[6] = stats.per_person[person].unmitigated_isolytic; 
      pies[display_person].datasets[2].weight = 1.0 / big_total * (stats.per_person[person].hull + stats.per_person[person].unmitigated_isolytic);
    }
    return pies;
  }
  get_stats(): IStatsResult {
    // must clear all stats before now
    this.total_damage = bake_pie();
    this.unmitigated_damage = bake_pie();
    this.hull_damage = bake_pie();
    this.damage_per_round = draw_line();

    this.total_damage.labels = [];
    this.unmitigated_damage.labels = [];
    this.hull_damage.labels = [];
    this.damage_per_round.labels = [];

    let attackers: Array<string> = [];
    let attacker_ships: IRow = {};
    let stats_per_person: IDamageTable = {};
    let stats_per_person_per_round: Array<IDamageTable> = [];
    let combat = this.tables.length - 1;
    let ship_candidates: Array<IShipCandidate> = [];
    let ship_deaths: Array<string> = [];
    // console.log(this.tables);
    for (let table of this.tables) {
      if (table[0]["Fleet Type"]) {
        for (let row of table) {
          let candidate: IShipCandidate = {
            strength: parseInt(row["Attack"]) + parseInt(row["Defense"]) + parseInt(row["Health"]),
            name_candidates: this.ability_to_ship[row["Ship Ability"]],
            name: "",
            captain: this.ability_to_captain[row["Captain Maneuver"]],
            officer1: this.ability_to_officer[row["Officer Two Ability"]],
            officer2: this.ability_to_officer[row["Officer Three Ability"]],
            player: "",
            is_enemy: row["Fleet Type"].includes("Enemy"),
            is_alive: true
          };
          ship_candidates.push(candidate);
        }
        break;
      }
    }
    for (let row of this.tables[combat]) {
      let attacker_name = NAMES[row["Attacker Name"]] || row["Attacker Name"];
      if(attacker_name.toLowerCase() !== row["Attacker Ship"].toLowerCase()) {
        attacker_name = `${attacker_name}(${row["Attacker Ship"]})`;
      }
      let ship_name = row["Attacker Ship"];

      let target_name = row["Target Name"]; //NAMES[row["Target Name"]] || row["Target Name"];
      let target_ship_name = row["Target Ship"];

      if (!attacker_ships[attacker_name]) {
        attacker_ships[attacker_name] = ship_name;
      }
      if (target_name != "--" && !attacker_ships[target_name]) {
        attacker_ships[target_name] = target_ship_name;
      }

      if (row["Type"] === "Combatant Destroyed") {
        ship_deaths.push(`${row["Attacker Name"]}.${row["Attacker Ship"]}`);
      }
      if (row["Type"] === "Attack") {
        let target_name = NAMES[row["Target Name"]] || row["Target Name"];
        if(target_name.toLowerCase() !== row["Target Ship"].toLowerCase()) {
          target_name = `${target_name}(${row["Target Ship"]})`;
        }
        let total_damage = parseInt(row["Total Damage"]);
        let isolytic_damage = parseInt(row["Total Isolytic Damage"])
        let unmitigated_damage =
          parseInt(row["Total Damage"]) - parseInt(row["Mitigated Damage"]);
        let unmitigated_isolytic_damage = parseInt(row["Total Isolytic Damage"] || "0") - parseInt(row["Mitigated Isolytic Damage"] || "0")
        let hull_damage = parseInt(row["Hull Damage"]);
        let round = parseInt(row["Round"]);
        if (!attackers.includes(target_name)) {
          stats_per_person[target_name] = make_per_person_row();
        }
        if (!attackers.includes(attacker_name)) {
          attackers.push(attacker_name);
          stats_per_person[attacker_name] = make_per_person_row();

          this.total_damage.labels.push(attacker_name)
          this.unmitigated_damage.labels.push(attacker_name)
          this.hull_damage.labels.push(attacker_name)

          this.total_damage.datasets[0].data.push(0)
          this.unmitigated_damage.datasets[0].data.push(0)
          this.hull_damage.datasets[0].data.push(0)
        }
        this.total_damage.datasets[0].data[this.total_damage.labels.indexOf(attacker_name)] += total_damage
        this.unmitigated_damage.datasets[0].data[this.unmitigated_damage.labels.indexOf(attacker_name)] += unmitigated_damage
        this.hull_damage.datasets[0].data[this.hull_damage.labels.indexOf(attacker_name)] += hull_damage

        stats_per_person[attacker_name].total += total_damage;
        if (row["Critical Hit?"] == "YES") {
          stats_per_person[attacker_name].critical += total_damage;
        }
        if (target_name != "") {
          stats_per_person[target_name].hull_recv += hull_damage;
          stats_per_person[target_name].total_recv += total_damage;
        }

        stats_per_person[attacker_name].mitigated += parseInt(row["Mitigated Damage"]);
        stats_per_person[attacker_name].mitigated_isolytic += parseInt(row["Mitigated Isolytic Damage"]);
        stats_per_person[attacker_name].shield += parseInt(row["Shield Damage"]);
        stats_per_person[attacker_name].unmitigated += unmitigated_damage;
        stats_per_person[attacker_name].hull += hull_damage;
        stats_per_person[attacker_name].isolytic += isolytic_damage;
        stats_per_person[attacker_name].unmitigated_isolytic += unmitigated_isolytic_damage;

        while (stats_per_person_per_round.length < round) {
          stats_per_person_per_round.push({});
        }
        console.log(round, round - 1);
        if (!stats_per_person_per_round[round - 1][attacker_name]) {
          stats_per_person_per_round[round - 1][attacker_name] = make_per_person_row();
        }
        stats_per_person_per_round[round - 1][attacker_name].total +=
          total_damage;
        stats_per_person_per_round[round - 1][attacker_name].unmitigated +=
          unmitigated_damage;
        stats_per_person_per_round[round - 1][attacker_name].isolytic +=
          isolytic_damage;
        stats_per_person_per_round[round - 1][attacker_name].unmitigated_isolytic +=
          unmitigated_isolytic_damage;
        stats_per_person_per_round[round - 1][attacker_name].hull +=
          hull_damage;
      }
    }
    for (let attacker in stats_per_person) {
      stats_per_person[attacker].critical_pct = (100.0 / stats_per_person[attacker].total * stats_per_person[attacker].critical).toFixed(2) + "%";
      stats_per_person[attacker].mitigated_pct = (100.0 / stats_per_person[attacker].total_recv * stats_per_person[attacker].mitigated).toFixed(2) + "%";
      stats_per_person[attacker].isolytic_pct = (100.0 / stats_per_person[attacker].total * stats_per_person[attacker].isolytic).toFixed(2) + "%";
    }

    this.damage_per_round.labels = Array.from(
      { length: stats_per_person_per_round.length },
      (_, i) => String(i + 1)
    );
    this.damage_per_round.datasets = [];
    for (let attacker in attackers) {
      this.damage_per_round.datasets.push({
        label: attackers[attacker],
        borderColor: background_colors[attacker],
        data: stats_per_person_per_round.map((item) =>
          item[attackers[attacker]]
            ? item[attackers[attacker]].total
            : 0
        ),
        cubicInterpolationMode: "monotone",
      });
    }
    return {
      per_person: stats_per_person,
      attackers: attackers
    };
  }
}

export default BattleLog;
